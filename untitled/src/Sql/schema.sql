create table driver
(
    first_name        char(30) default 'Default_Firstname',
    id                bigserial primary key,
    last_name         char(30) default 'Default_Lastname',
    phone             integer,
    expirience        integer,
    age               integer check ( age >= 18 and age <= 150 ),
    have_license      bool,
    category_of_drive char(4),
    rait              integer check (rait >= 0 and rait <= 5)
);
insert into driver (first_name, last_name, phone, expirience, age, have_license, category_of_drive, rait)
values ('Иван', 'Иванов', '11111', 1, 22, true, 'b', 4);
insert into driver (first_name, last_name, phone, expirience, age, have_license, category_of_drive, rait)
values ('Сергей', 'Сергеев', '22222', 6, 25, true, 'b', 5);
insert into driver (first_name, last_name, phone, expirience, age, have_license, category_of_drive, rait)
values ('Джон', 'Джонов', '33333', 5, 32, true, 'bc', 4);
insert into driver (first_name, last_name, phone, expirience, age, have_license, category_of_drive, rait)
values ('Петр', 'Петров', '5555', 1, 25, false, '0', 0);
insert into driver (first_name, last_name, phone, expirience, age, have_license, category_of_drive, rait)
values ('Александра', 'Александрова', '69699', 8, 25, true, 'b', 5);

create table Car
(
    model    char(100),
    color    char(100),
    number   char(10),
    id_owner integer not null,
    car_id   serial primary key,
    foreign key (id_owner) references driver (id)
);
insert into Car (model, color, number, id_owner)
values ('Авенсис', 'Белая', 'АА001Ф', 1);
insert into Car (model, color, number, id_owner)
values ('Нива', 'Черная', 'ЖЗ009П', 1);
insert into Car (model, color, number, id_owner)
values ('Киа Сид', 'Розовая', 'AA789H', 2);
insert into Car (model, color, number, id_owner)
values ('Лада 2110', 'Баклажан', 'АА007Ф', 3);
insert into Car (model, color, number, id_owner)
values ('Мерседес Е200', 'Белая', 'ВО001Р', 4);
insert into Car (model, color, number, id_owner)
values ('БМВ Х5', 'Синий', 'ЛО001Р', 4);



create table trip
(
    id_driver    integer not null,
    foreign key (id_driver) references driver (id),
    id_car       integer not null,
    foreign key (id_car) references car (car_id),
    date         date,
    time_at_trip time
);

insert into trip
values (1, 1, '10-11-22', '02:00'),
       (2, 2, '11-11-22', '03:00'),
       (3, 3, '10-11-22', '01:00'),
       (5, 4, '10-11-22', '00:30'),
       (5, 5, '11-11-22', '07:00'),
       (5, 2, '18-11-22', '03:00'),
       (2, 6, '10-11-22', '02:00');
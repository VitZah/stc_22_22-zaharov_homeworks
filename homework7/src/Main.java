public class Main {

    public static void completeAllTask(Task[] finishedTasks) {
        for ( int i = 0; i < finishedTasks.length; i++){
            finishedTasks[i].complete();
        }
    }

    public static void main(String[] args) {

Task task1  =  new EvenNumbersPrintTask(1,10);
Task task2  = new OddNumbersPrintTask(3,6);

Task [] finishedTask = {task1, task2};
completeAllTask(finishedTask);




    }
}
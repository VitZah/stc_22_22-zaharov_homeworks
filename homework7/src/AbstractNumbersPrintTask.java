public abstract class AbstractNumbersPrintTask implements Task {

    protected int from;
    protected int to;


    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }
    public AbstractNumbersPrintTask( int from, int to) {
        this.from = from;
        this.to = to;
    }


    public abstract void complete();
}

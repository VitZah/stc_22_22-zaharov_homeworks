import org.jetbrains.annotations.NotNull;

public class Main {
    public static int sumofelements(int[] a, int from, int to) {
        int sum = 0;

        if (from <= to) {
            for (int i = from; i <= to; i++) {
                sum = sum + a[i];
            }
        } else {
            sum = -1;
        }
        return sum;
    }

    public static boolean isEven(int a) {
        return   a % 2 == 0;
             }


    public static void main(String[] args) {
        // взят произвольный массив
        int[] array = {9, 6, 3, 6, 8, 5};
        int sum1 = sumofelements((array), 0, 4);
        System.out.println(" Сумма выбранных чисел массива =" + sum1);
        System.out.println(" четные числа массива " );
        for (int i = 0; i < array.length; i++) {
            boolean condition = isEven(array [i]);
                       if (condition == true) {
                System.out.print(array[i] + "  ");
            }
        }
    }
}


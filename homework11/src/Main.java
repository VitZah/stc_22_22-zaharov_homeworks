import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {


        CarsRepository carsRepository = new CarRepositoryFileBasedImpl("src/input.txt");


        System.out.println(carsRepository.findAllCountUniqueNames());
        System.out.println(carsRepository.findAveragePriceOfModel("Camry"));
        System.out.println(carsRepository.findByBlackColorOrNew());
        System.out.println(carsRepository.findColorCarWithMinPrice());
    }
}

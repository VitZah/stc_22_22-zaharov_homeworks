import java.util.List;

public interface CarsRepository {


   List <String> findByBlackColorOrNew();

    String findColorCarWithMinPrice();

    long findAllCountUniqueNames();

    //double findMinPrice();

    double findAveragePriceOfModel(String modelname);


}

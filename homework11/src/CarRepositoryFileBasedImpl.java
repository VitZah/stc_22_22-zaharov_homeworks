import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Comparator;
import java.util.List;
import java.util.ListResourceBundle;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarRepositoryFileBasedImpl implements CarsRepository {
    private final String fileName;

    public CarRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Car> stringToCarMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer price = Integer.parseInt(parts[4]);
        return new Car(number, model, color, mileage, price);
    };


    private static final Function<Car, String> carToStringMapper = car ->
            car.getNumber(); //"|" + car.getModel() + "|" + car.getColor() + "|" + car.getMileage()+ "|" + car.getPrice();

    public List<Car> findAll() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToCarMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override

    public List<String> findByBlackColorOrNew() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            return bufferedReader
                    .lines()
                    .map(stringToCarMapper)
                    .filter(Car -> Car.getColor().equals("Black") || 0 == Car.getMileage())
                    .map(Car::getNumber)
                    .collect(Collectors.toList());


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public String findColorCarWithMinPrice() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparingDouble(Car::getPrice))
                    .map(Car::getColor)
                    .get();


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public long findAllCountUniqueNames() {

        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToCarMapper)
                    .filter(Car -> Car.getPrice() >= 700 && Car.getPrice() <= 800)
                    .distinct()
                    .count();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public double findAveragePriceOfModel(String modelname) {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToCarMapper)
                    .filter(Car -> Car.getModel().equals(modelname))
                    .mapToDouble(Car::getPrice)
                    .average()
                    .getAsDouble();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


}

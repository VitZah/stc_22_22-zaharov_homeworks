import java.util.HashMap;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("введите слова, разделите их пробелом");


        String[] words = scanner.nextLine().split(" ");
        HashMap<String, Integer> wordsMap = new HashMap<String, Integer>();
        for (int i = 0; i < words.length; i++) {
            if (wordsMap.containsKey(words[i])) {
                wordsMap.replace(words[i], wordsMap.get(words[i]) + 1);
            } else {
                wordsMap.put(words[i], 1);
            }
        }
        String maxRepeat = null;
        int a = 0;
        for (String v : wordsMap.keySet()) {
            if (a < wordsMap.get(v)) {
                a = wordsMap.get(v);
                maxRepeat = v;
            }
        }
        System.out.println("Самое повторяющееся слово: " + maxRepeat + " (встречается " + wordsMap.get(maxRepeat) + " раз)");
    }
}




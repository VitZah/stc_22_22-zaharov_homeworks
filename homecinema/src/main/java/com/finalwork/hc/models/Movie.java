package com.finalwork.hc.models;

import javax.persistence.*;
import lombok.*;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "movieSession")
@ToString(exclude = "movieSession")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String moviesName;
    private String description;
    private String mainActor;




   @ManyToMany
           @JoinTable(joinColumns = {@JoinColumn(name = "movie_id", referencedColumnName = "id")},
           inverseJoinColumns = {@JoinColumn(name = "movie_session_id", referencedColumnName = "id")})

    private Set <MovieSession> movieSessions;


}
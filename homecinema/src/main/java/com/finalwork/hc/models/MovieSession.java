package com.finalwork.hc.models;

import javax.persistence.*;
import lombok.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"movies", "accounts"})
@Entity
@Table(name = "movie_session")
public class MovieSession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;


    private LocalDate date;


    @ManyToMany (mappedBy = "movieSessions", fetch = FetchType.EAGER)
    private Set<Movie> movies;

    @ManyToMany(mappedBy = "movieSessions", fetch = FetchType.EAGER)
    private Set<User> accounts;


}
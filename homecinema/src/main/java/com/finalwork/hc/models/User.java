package com.finalwork.hc.models;

import javax.persistence.*;
import lombok.*;

import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"movieSessions"})
@ToString(exclude = {"movieSessions"})
@Builder
@Entity
@Table(name = "account")
public class User {

    public enum State {
         CONFIRMED, DELETED
    }

    public enum Role {
        USER, ADMIN
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;



    @Column(unique = true, nullable = false)
    private String email;

    private String password;



    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "account_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_Session_id", referencedColumnName = "id")})
    private Set<MovieSession> movieSessions;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @Enumerated(value = EnumType.STRING)
    private Role role;

}
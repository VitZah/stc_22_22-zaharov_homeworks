package com.finalwork.hc.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.finalwork.hc.models.MovieSession;
import com.finalwork.hc.security.CustomUserDetails;
import com.finalwork.hc.services.MovieSessionsService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping("/movieSessions")

public class MovieSessionController {

    private final MovieSessionsService movieSessionsService;

    @GetMapping
    public String getMovieSessionsPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {

        List<MovieSession> movieSessions = movieSessionsService.getAllMovieSessions();
        model.addAttribute("movieSessionsList",movieSessions);
        model.addAttribute("role", customUserDetails.getUser().getRole());

    return "movieSessions_page";
    }

    @PostMapping("/{movieSession-id}/accounts")
    public String addAccountToMovieSession(@PathVariable("movieSession-id") Long movieSessionId,
                                           @RequestParam("account-id") Long accountId) {
        movieSessionsService.addAccountToMovieSession(movieSessionId, accountId);
        return "redirect:/movieSessions/" + movieSessionId;
    }

    @GetMapping("/{movieSession-id}")
    public String getMovieSessionPage(@PathVariable("movieSession-id")@AuthenticationPrincipal CustomUserDetails customUserDetails, Long id, Model model) {
        model.addAttribute("movieSession", movieSessionsService.getMovieSession(id));
        model.addAttribute("notInMovieSessionAccounts", movieSessionsService.getNotInMovieSessionAccounts(id));
        model.addAttribute("inMovieSessionAccounts", movieSessionsService.getInMovieSessionAccounts(id));
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("inMovieSessionMovie", movieSessionsService.getInMovieSessionMovies(id));
        return "movieSession_page";
    }

    @PostMapping("/{movieSession-id}/movies")
    public String addMovieToMovieSession(@PathVariable("movieSession-id") Long movieSessionId,
                                           @RequestParam("movie-id") Long movieId) {
        movieSessionsService.addMovieToMovieSession(movieSessionId,movieId);
        return "redirect:/movieSessions/" + movieSessionId;
    }

        @PostMapping("/movieSessions")
        public String addMovieSession (MovieSession movieSession){
            movieSessionsService.addMovieSession(movieSession);
            return "redirect:/movieSessions";
        }

    }










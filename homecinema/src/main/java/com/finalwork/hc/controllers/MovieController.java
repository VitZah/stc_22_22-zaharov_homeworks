package com.finalwork.hc.controllers;

import com.finalwork.hc.models.Movie;
import com.finalwork.hc.security.CustomUserDetails;
import com.finalwork.hc.services.MovieService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;


@RequiredArgsConstructor
@Controller
public class MovieController {
    private final MovieService movieService;

@GetMapping("/movies")
public String getMoviesPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model){
ModelAndView modelAndView = new ModelAndView("movies_page");
List<Movie> movies = movieService.getAllMovies();
model.addAttribute("role", customUserDetails.getUser().getRole());
model.addAttribute("moviesList",movies);
return "movies_page";
}
@PostMapping("/movies")
public String addMovie(Movie movie){
    movieService.addMovie(movie);
    return"redirect:/movies";
}
@GetMapping("movies/{movie-id}")
    public String getMoviePage(@PathVariable("movie-id") Long id,Model model) {
model.addAttribute("movie",movieService.getMovie(id));
return "movie_page";
}


}




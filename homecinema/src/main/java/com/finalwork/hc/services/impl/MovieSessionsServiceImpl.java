package com.finalwork.hc.services.impl;
import com.finalwork.hc.models.Movie;
import com.finalwork.hc.repositories.MovieRepository;
import com.finalwork.hc.services.MovieSessionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.finalwork.hc.models.MovieSession;
import com.finalwork.hc.models.User;
import com.finalwork.hc.repositories.MovieSessionRepsitory;
import com.finalwork.hc.repositories.UsersRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class MovieSessionsServiceImpl implements MovieSessionsService {


    private final MovieSessionRepsitory movieSessionsRepsitory;
    private final UsersRepository usersRepository;
    private final MovieRepository movieRepository;

    @Override
    public void addAccountToMovieSession(Long movieSessionId, Long accountId) {
        MovieSession movieSession = movieSessionsRepsitory.findById(movieSessionId).orElseThrow();
        User account = usersRepository.findById(accountId).orElseThrow();

        account.getMovieSessions().add(movieSession);

        usersRepository.save(account);
    }
    public void addMovieToMovieSession(Long movieSessionId, Long movieId) {
        MovieSession movieSession = movieSessionsRepsitory.findById(movieSessionId).orElseThrow();
        Movie movie = movieRepository.findById(movieId).orElseThrow();
        movie.getMovieSessions().add(movieSession);

        movieRepository.save(movie);
    }
    @Override
    public MovieSession getMovieSession(Long movieSessionId) {
        return movieSessionsRepsitory.findById(movieSessionId).orElseThrow();
    }
    public void addMovieSession(MovieSession movieSession) {
        MovieSession newMovieSession = MovieSession.builder()
                .title(movieSession.getTitle())
                .date(movieSession.getDate())
                .build();

        movieSessionsRepsitory.save(newMovieSession);
    }

    @Override
    public List<MovieSession> getAllMovieSessions() {
        return movieSessionsRepsitory.findAll();
    }


    @Override
    public List<User> getNotInMovieSessionAccounts(Long movieSessionId) {
        MovieSession movieSession = movieSessionsRepsitory.findById(movieSessionId).orElseThrow();
        return usersRepository.findAllByMovieSessionsNotContainsAndState(movieSession, User.State.CONFIRMED);
    }

    @Override
    public List<User> getInMovieSessionAccounts(Long movieSessionId) {
        MovieSession movieSession = movieSessionsRepsitory.findById(movieSessionId).orElseThrow();
        return usersRepository.findAllByMovieSessionsContains(movieSession);
    }



    public List<Movie> getInMovieSessionMovies(Long movieSessionId){
        MovieSession movieSession = movieSessionsRepsitory.findById(movieSessionId).orElseThrow();
        return movieRepository.findAllByMovieSessionsContains(movieSession);
    }

    }




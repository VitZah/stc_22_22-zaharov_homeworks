package com.finalwork.hc.services;

import com.finalwork.hc.models.Movie;
import com.finalwork.hc.models.MovieSession;
import com.finalwork.hc.models.User;

import java.util.List;

public interface MovieSessionsService {
    void addAccountToMovieSession(Long movieSessionId, Long accountId);

    MovieSession getMovieSession(Long movieSessionId);

    List<User> getNotInMovieSessionAccounts(Long movieSessionId);

    List<User> getInMovieSessionAccounts(Long movieSessionId);

    List<Movie> getInMovieSessionMovies(Long movieSessionId);
    void addMovieSession (MovieSession movieSession);

    List<MovieSession> getAllMovieSessions();

   void addMovieToMovieSession(Long movieSessionId, Long movieId);

}


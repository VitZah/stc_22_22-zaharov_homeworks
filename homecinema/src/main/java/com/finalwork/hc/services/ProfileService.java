package com.finalwork.hc.services;

import com.finalwork.hc.models.User;
import com.finalwork.hc.security.CustomUserDetails;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);
}

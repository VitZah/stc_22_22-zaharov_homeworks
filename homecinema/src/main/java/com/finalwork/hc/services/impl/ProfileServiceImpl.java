package com.finalwork.hc.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.finalwork.hc.models.User;
import com.finalwork.hc.repositories.UsersRepository;
import com.finalwork.hc.security.CustomUserDetails;
import com.finalwork.hc.services.ProfileService;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final UsersRepository usersRepository;

    @Override
    public User getCurrent(CustomUserDetails userDetails) {
        return usersRepository.findById(userDetails.getUser().getId()).orElseThrow();
    }
}
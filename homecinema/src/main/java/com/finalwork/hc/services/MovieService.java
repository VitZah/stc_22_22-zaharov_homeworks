package com.finalwork.hc.services;
import com.finalwork.hc.models.MovieSession;
import com.finalwork.hc.models.User;
import com.finalwork.hc.models.Movie;
import java.util.List;




public interface MovieService {

    List<Movie> getAllMovies();
    void addMovieToMovieSession(Long movieSessionId, Long movieId);

    void deleteMovieFromMovieSession(Long movieSessionId, Long movieId);

    Movie getMovie(Long movieId);


    void addMovie(Movie movie);
}
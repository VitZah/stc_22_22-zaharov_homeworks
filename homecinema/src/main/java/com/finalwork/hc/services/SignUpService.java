package com.finalwork.hc.services;

import com.finalwork.hc.dto.UserForm;


public interface SignUpService {
    void signUp(UserForm userForm);
}

package com.finalwork.hc.services.impl;

import com.finalwork.hc.models.Movie;
import com.finalwork.hc.models.MovieSession;
import com.finalwork.hc.repositories.MovieRepository;
import com.finalwork.hc.repositories.MovieSessionRepsitory;
import com.finalwork.hc.services.MovieService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@RequiredArgsConstructor
@Service

public class MovieServiceImpl implements MovieService {

    private final MovieSessionRepsitory movieSessionRepsitory;

    private final MovieRepository movieRepository;


    @Override
    public Movie getMovie(Long movieId) {
        return movieRepository.findById(movieId).orElseThrow();
    }

    @Override
    public void addMovie(Movie movie) {
        Movie newMovie = Movie.builder()
                .moviesName(movie.getMoviesName())
                .description(movie.getDescription())
                .mainActor(movie.getMainActor())
                .build();

        movieRepository.save(newMovie);
    }


    @Override
    public List<Movie> getAllMovies(){
        return movieRepository.findAll();
    }



    @Override
    public void addMovieToMovieSession(Long movieSessionId, Long movieId) {
        Movie movie = movieRepository.findById(movieId).orElseThrow();
        MovieSession movieSession = movieSessionRepsitory.findById(movieId).orElseThrow();
        movieSession.getMovies().add(movie);

    }

    @Override
    public void deleteMovieFromMovieSession(Long movieSessionId, Long movieId) {
        Movie movie = movieRepository.findById(movieId).orElseThrow();
        MovieSession movieSession = movieSessionRepsitory.findById(movieId).orElseThrow();
        movieSession.getMovies().remove(movie);
    }

}

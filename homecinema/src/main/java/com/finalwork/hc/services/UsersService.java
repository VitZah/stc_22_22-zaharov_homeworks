package com.finalwork.hc.services;


import com.finalwork.hc.dto.UserForm;
import com.finalwork.hc.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void addUser(UserForm user);

    User getUser(Long id);

    void updateUser(Long userId, UserForm user);

    void deleteUser(Long userId);
}

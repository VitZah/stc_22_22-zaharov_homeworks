package com.finalwork.hc.repositories;


import com.finalwork.hc.models.MovieSession;
import com.finalwork.hc.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import com.finalwork.hc.models.Movie;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {
    List<Movie> findAllByMovieSessionsContains(MovieSession movieSession);
}

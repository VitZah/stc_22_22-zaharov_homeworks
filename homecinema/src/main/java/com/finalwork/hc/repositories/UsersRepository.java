package com.finalwork.hc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.finalwork.hc.models.MovieSession;
import com.finalwork.hc.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByMovieSessionsNotContainsAndState(MovieSession movieSession, User.State state);

    List<User> findAllByMovieSessionsContains(MovieSession movieSession);

    Optional<User> findByEmail(String email);
}

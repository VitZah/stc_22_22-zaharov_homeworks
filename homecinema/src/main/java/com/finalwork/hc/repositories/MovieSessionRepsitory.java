package com.finalwork.hc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.finalwork.hc.models.MovieSession;

public interface MovieSessionRepsitory extends JpaRepository<MovieSession, Long> {
}



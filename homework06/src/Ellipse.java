public class Ellipse extends Circle {
    private double radius2;

    public Ellipse(int x, int y, double radius1, double radius2) {
        super(x, y, radius1);
        this.radius2 = radius2;
    }

    public void setRadius(double radius2) {
        if (radius2 > 0) {
            this.radius2 = radius2;
        } else {
            this.radius2 = 0.01;
            System.out.println("Значение стороны не может быть меньше 0.01");
        }
    }

    public double getPerimetr() {
        perimetr = 2 * Math.PI * Math.sqrt((radius2 * radius2 + radius1 * radius1) / 8);
        System.out.println("Периметр эллипса =  " + perimetr);
        return perimetr;

    }

    public double getFigurarea() {
        figurearea = Math.PI * radius1 * radius2;
        System.out.println("Площадь эллипса = " + figurearea);
        return figurearea;
    }
}

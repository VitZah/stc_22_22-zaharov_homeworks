public class Main {

    public static void getAllPerimetr(Figure[] figures) {
        for (int i = 0; i < figures.length; i++) {
            figures[i].getPerimetr();}
    }
    public static void getAllFigurearea(Figure[] figures) {
        for (int i = 0; i < figures.length; i++) {
            figures[i].getFigurarea();
        }
    }

    public static void main(String[] args) {

        Square f1 = new Square(5, 6, 10);
        Rectangle f2 = new Rectangle(4, 5, 3, 6);
        Circle f3 = new Circle(4, 8, 5);
        Ellipse f4 = new Ellipse(7, 10, 6, 8);
        Figure[] figures = {f1, f2, f3, f4};

getAllPerimetr(figures);
getAllFigurearea(figures);

// Передвижение любой фигуры
        f1.moveFigure(1,5);
    }

}





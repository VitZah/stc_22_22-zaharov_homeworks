public class Square extends Figure {

    protected double side1;

    // конструктор фигуры
    public Square(int x, int y, double side1) {
        super(x, y);
        this.side1 = side1;
    }


    public void setSide1(int side1) {
        if (side1 > 0) {
            this.side1 = side1;
        } else {
            this.side1 = 1;
            System.out.println("Значение стороны не может быть меньше 1");
        }
    }

    double perimetr = 0;

    public double  getPerimetr() {
        perimetr = side1 * 4;
        System.out.println("Периметр квадрата =  " + perimetr);
        return  perimetr;

    }

    double figurearea = 0;

    public double getFigurarea() {
        figurearea = side1 * side1;
        System.out.println("Площадь квадрата = " + figurearea);
        return figurearea;
    }

}



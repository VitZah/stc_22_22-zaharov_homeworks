import java.util.Scanner;

public abstract class Figure {
    // координаты центра
    protected int x;
    protected int y;

    // конструктор фигуры
    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    double perimetr = 0;

    public  abstract double   getPerimetr() ;



    double figurearea = 0;

    public abstract double getFigurarea();

    public void moveFigure(int x, int y) {
        this.x = x;
        this.y = y;

        System.out.println(" Фигура перемещена, новые координаты центра фигуры  " + x + "  " + y);
    }
}

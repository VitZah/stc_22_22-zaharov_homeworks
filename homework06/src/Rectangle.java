public class Rectangle extends Square {
    private double side2;

    public Rectangle(int x, int y, double side1, double side2) {
        super(x, y, side1);
        this.side2 = side2;
    }

    public void setSide2(double side1) {
        if (side2 > 0) {
            this.side2 = side1;
        } else {
            this.side2 = 1;
            System.out.println("Значение стороны не может быть меньше 1");
        }
    }

    public double getPerimetr() {
        perimetr = 2 * (side1 + side2);
        System.out.println("Периметр прямоугольника =  " + perimetr);
        return perimetr;

    }

    public double getFigurarea() {
        figurearea = side1 * side2;
        System.out.println("Площадь прямоугольника = " + figurearea);
        return figurearea;
    }


}

public class Circle extends Figure {
    protected double radius1;

    protected Circle(int x, int y, double radius1) {
        super(x, y);
        this.radius1 = radius1;
    }

    public void setRadius(double radius1) {
        if (radius1 > 0) {
            this.radius1 = radius1;
        } else {
            this.radius1 = 0.01;
            System.out.println("Значение стороны не может быть меньше 0.01");
        }
    }

    public double getPerimetr() {
        perimetr = 2 * Math.PI * radius1;
        System.out.println("Периметр круга =  " + perimetr);
        return perimetr;
    }

    public double getFigurarea() {
        figurearea = Math.PI * radius1 * radius1;
        System.out.println("Площадь круга = " + figurearea);
        return figurearea;

    }
}

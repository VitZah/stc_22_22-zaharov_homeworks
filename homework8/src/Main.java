import org.w3c.dom.ls.LSOutput;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        ArrayTask task1 = ((array, from, to) -> {
            ;
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }

            return sum;


        });


        ArrayTask task2 = ((array, from, to) -> {
            int maxnumber = array[from];
            for (int i = from; i <= to; i++) {
                if (array[i] > maxnumber) maxnumber = array[i];

            }
            int sum = 0;
            while (maxnumber != 0) {
                sum += maxnumber % 10;
                maxnumber /= 10;
            }

            return sum;
        });

        int[] array1 = {12, 32, 13, 69, 24, 15};
        ArraysTasksResolver.resolveTask(array1, task1, 0, 3);

        ArraysTasksResolver.resolveTask(array1, task2, 0, 2);
    }

}


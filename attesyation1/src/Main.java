import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {


        ProductRepository productRepository = new ProductRepositoryByFileBasedImpl("src/input.txt");

        System.out.println(productRepository.findById(2));

        System.out.println(productRepository.findAllByTitle("кос"));

        //System.out.println(productRepository.expirience(2));


        Product apricot = productRepository.findById(1);
        apricot.setPrice(10.0);
        apricot.setAmount(100.0);
        System.out.println(apricot);


    }
}
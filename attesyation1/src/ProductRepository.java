import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface ProductRepository {
    List<Product> findAll();

    List<Product> findAllByTitle(String title);

   Product findById(Integer id) throws FileNotFoundException;

    void update(Product product);
}



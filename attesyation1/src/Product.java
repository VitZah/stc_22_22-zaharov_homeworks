public class Product {
    private Integer id;
    private String productName;
    private Double price;
    private Double  amount;

    public Product(Integer id, String productName, Double  price, Double  amount) {
        this.id = id;
        this.productName = productName;
        this.price = price;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                ", amount=" + amount +
                '}';
    }

    public  Integer getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public Double  getPrice() {
        return price;
    }

    public void setPrice(Double  price) {
        this.price = price;
    }

    public Double  getAmount() {
        return amount;
    }

    public void setAmount(Double  amount) {
        this.amount = amount;
    }
}

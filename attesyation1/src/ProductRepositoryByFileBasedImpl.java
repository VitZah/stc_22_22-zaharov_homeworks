
import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;


public class ProductRepositoryByFileBasedImpl implements ProductRepository {

    private final String fileName;

    public ProductRepositoryByFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String productName = parts[1];
        Double price = Double.parseDouble(parts[2]);
        Double amount = Double.parseDouble(parts[3]);

        return new Product(id, productName, price, amount);
    };

    private static final Function<Product, String> productToStringMapper = product ->
            product.getId() + "|" + product.getProductName() + "|" + product.getPrice() + "|" + product.getAmount();

    @Override
    public List<Product> findAll() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Product> findAllByTitle(String title) {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getProductName().contains(title))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Product findById(Integer id) throws FileNotFoundException {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> id == product.getId())
                    .findFirst()
                    .orElse(null);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    //  третий метод полноценно не получился, оставляю его пустым.
    @Override
    public void update(Product product) {

    }
}
















